import uuid

import boto3
import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_flow_log(hub, ctx, aws_ec2_vpc):
    # create flow_log
    flow_log_id = "idem-test-flow-log-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": flow_log_id},
    ]
    bucket_name = "test" + str(uuid.uuid4())
    session: boto3.session.Session = hub.tool.boto3.session.get(botocore_session=None)
    bucket_client = session.client(
        service_name="s3",
        region_name=ctx.acct.get("region_name"),
        api_version=ctx.acct.get("api_version"),
        use_ssl=ctx.acct.get("use_ssl", True),
        endpoint_url=ctx.acct.get("endpoint_url"),
        aws_access_key_id=ctx.acct.get("aws_access_key_id"),
        aws_secret_access_key=ctx.acct.get("aws_secret_access_key"),
        aws_session_token=ctx.acct.get("aws_session_token"),
        verify=ctx.acct.get("verify"),
    )
    bucket_client.create_bucket(Bucket=bucket_name)

    ret = await hub.states.aws.ec2.flow_log.present(
        ctx=ctx,
        name=flow_log_id,
        resource_type="VPC",
        traffic_type="ALL",
        log_destination_type="s3",
        iam_role=None,
        log_group_name=None,
        log_destination="arn:aws:s3:::" + bucket_name,
        log_format="${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport} ${dstport} ${protocol} "
        "${packets} ${bytes} ${start} ${end} ${action} ${log-status}",
        max_aggregation_interval=600,
        resource_ids=[aws_ec2_vpc.get("VpcId")],
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["name"] == flow_log_id
    assert ret["new_state"]["Tags"] == tags
    assert bucket_name in ret["new_state"]["LogDestination"]
    created_flow_log_id = ret["new_state"]["FlowLogId"]

    # verify that created flow log is present
    describe_ret = await hub.states.aws.ec2.flow_log.describe(ctx)
    assert created_flow_log_id in describe_ret

    # update tags
    new_tags = [
        {"Key": "new-name", "Value": created_flow_log_id},
    ]
    ret = await hub.states.aws.ec2.flow_log.present(
        ctx,
        name=created_flow_log_id,
        iam_role=None,
        log_group_name=None,
        resource_type="VPC",
        traffic_type="ALL",
        log_destination_type="s3",
        log_destination="arn:aws:s3:::" + bucket_name,
        log_format="${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport} ${dstport} ${protocol} "
        "${packets} ${bytes} ${start} ${end} ${action} ${log-status}",
        max_aggregation_interval=600,
        resource_ids=[aws_ec2_vpc.get("VpcId")],
        tags=new_tags,
    )
    assert ret["result"]
    assert "new_state" in ret
    assert ret["new_state"].get("Tags") == new_tags

    # Delete instance
    ret = await hub.states.aws.ec2.flow_log.absent(ctx, name=created_flow_log_id)
    assert f"Deleted '{created_flow_log_id}'" in ret["comment"]

    # Deleting the same instance again should state the same in comment
    ret = await hub.states.aws.ec2.flow_log.absent(ctx, name=created_flow_log_id)
    assert f"'{created_flow_log_id}' is already in deleted state." in ret["comment"]
    bucket_client.delete_bucket(Bucket=bucket_name)
