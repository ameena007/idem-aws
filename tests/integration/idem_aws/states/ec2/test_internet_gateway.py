import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_internet_gateway(hub, ctx, aws_ec2_vpc):
    # Create internet_gateway
    internet_gateway_id = "idem-test-internet-gateway-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": internet_gateway_id},
    ]

    vpc_id = aws_ec2_vpc.get("VpcId")
    # When vpc_id passed, resource is created and attached to vpc
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=internet_gateway_id,
        vpc_id=[vpc_id],
        tags=tags,
    )
    created_internet_gateway_id = ret["new_state"]["InternetGatewayId"]
    assert tags == ret["new_state"]["Tags"]
    assert vpc_id == ret["new_state"]["Attachments"][0].get("VpcId")
    assert (
        f"Created '{created_internet_gateway_id}' and attached to vpc '{vpc_id}'"
        in ret["comment"]
    )

    # Verify that created internet_gateway is present
    describe_ret = await hub.states.aws.ec2.internet_gateway.describe(ctx)
    assert created_internet_gateway_id in describe_ret
    assert "aws.ec2.internet_gateway.present" in describe_ret.get(
        created_internet_gateway_id
    )
    described_resource = describe_ret.get(created_internet_gateway_id).get(
        "aws.ec2.internet_gateway.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "vpc_id" in described_resource_map
    assert vpc_id == described_resource_map["vpc_id"][0]
    assert tags == described_resource_map.get("tags")

    # Invoke present without vpc_id to verify that there are no changes in attachments.
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=created_internet_gateway_id,
    )
    assert vpc_id == ret["new_state"]["Attachments"][0].get("VpcId")

    # Update tags
    new_tags = [
        {"Key": "new-name", "Value": created_internet_gateway_id},
    ]
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=created_internet_gateway_id,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert new_tags == ret["new_state"]["Tags"]

    # Attach to same vpc again
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=created_internet_gateway_id,
        vpc_id=[vpc_id],
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"'{created_internet_gateway_id}' is already attached to vpc '{vpc_id}'"
        in ret["comment"]
    )

    # Attach to different vpc, will first detach from attached vpc and then attach to this new vpc
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=created_internet_gateway_id,
        vpc_id=["invalid"],
        tags=new_tags,
    )
    assert not ret["result"], ret["comment"]
    assert (
        f"ClientError: An error occurred (InvalidVpcID.NotFound) when calling the AttachInternetGateway operation: VpcID invalid does not exist."
        in ret["comment"]
    )
    # verify that attachments are empty
    assert not ret["new_state"]["Attachments"]

    # Attach again to vpc
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=created_internet_gateway_id,
        vpc_id=[vpc_id],
    )
    assert ret["result"], ret["comment"]
    assert (
        f"'{created_internet_gateway_id}' attached to vpc '{vpc_id}'" in ret["comment"]
    )

    # Invoke delete
    ret = await hub.states.aws.ec2.internet_gateway.absent(
        ctx, name=created_internet_gateway_id
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"]["old"] and not ret["changes"].get("new")
    assert f"Deleted '{created_internet_gateway_id}'" in ret["comment"]

    # Delete the same instance again
    ret = await hub.states.aws.ec2.internet_gateway.absent(
        ctx, name=created_internet_gateway_id
    )
    assert f"'{created_internet_gateway_id}' already absent" in ret["comment"]
