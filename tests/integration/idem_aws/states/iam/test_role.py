import copy
import json
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_role(hub, ctx):
    role_temp_name = "idem-test-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Action": "s3:ListBucket","Resource": "arn:aws:s3:::example_bucket"}}'
    description = "Idem IAM role test description"
    max_session_duration = 3700
    tags = [{"Key": "Name", "Value": role_temp_name}]

    # Create IAM role with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.role.present(
        test_ctx,
        name=role_temp_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_temp_name == resource.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": ")
    ) == resource.get("assume_role_policy_document")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")

    # Create IAM role
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_temp_name == resource.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": ")
    ) == resource.get("assume_role_policy_document")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")
    assert role_temp_name == resource_id

    # Verify present w/o changes does not trigger an update
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"{role_temp_name} already exists" in ret["comment"]
    assert ret["old_state"] == ret["new_state"], "Should not update existing role"

    # Describe IAM role
    describe_ret = await hub.states.aws.iam.role.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.iam.role.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.iam.role.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert role_temp_name == described_resource_map.get("name")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": ")
    ) == described_resource_map.get("assume_role_policy_document")
    assert description == described_resource_map.get("description")
    assert max_session_duration == described_resource_map.get("max_session_duration")
    assert resource_id == described_resource_map.get("resource_id")
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.iam.role.present"], "tags", tags
    )

    # Test updating description, max_session_duration and adding tags
    description = "Idem IAM role test description updated"
    max_session_duration = 3800
    tags.append(
        {
            "Key": f"idem-test-iam-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-iam-value-{str(uuid.uuid4())}",
        }
    )

    # Update role with test flag
    ret = await hub.states.aws.iam.role.present(
        test_ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")

    # Update role with in real
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")

    # Test deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    old_resource = ret.get("old_state")
    resource = ret.get("new_state")
    assert old_resource.get("description") == resource.get("description")
    assert old_resource.get("max_session_duration") == resource.get(
        "max_session_duration"
    )
    assert tags == resource.get("tags")

    # Delete role with test flag
    ret = await hub.states.aws.iam.role.absent(
        test_ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.iam.role {role_temp_name}" in ret["comment"]

    # Delete IAM role - require name for deletion
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete IAM role again
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
