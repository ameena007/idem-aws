import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_log_group(hub, ctx):
    # Create log group
    log_group_temp_name = "idem-test-log-group-" + str(uuid.uuid4())
    tags = {"testKey1": "testValue1"}
    ret = await hub.states.aws.cloudwatch.log_group.present(
        ctx, name=log_group_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource_id = ret["new_state"]["logGroupName"]

    # Describe log group
    describe_ret = await hub.states.aws.cloudwatch.log_group.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.cloudwatch.log_group.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.cloudwatch.log_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert tags == described_resource_map.get("tags")

    # Update tags for log group
    new_tags = {"testKey1": "testValue2"}
    update_ret = await hub.states.aws.cloudwatch.log_group.present(
        ctx, name=resource_id, tags=new_tags
    )
    describe_ret = await hub.states.aws.cloudwatch.log_group.describe(ctx)
    described_resource = describe_ret.get(resource_id).get(
        "aws.cloudwatch.log_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert new_tags == described_resource_map.get("tags")

    # Delete log group
    ret = await hub.states.aws.cloudwatch.log_group.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.cloudwatch.log_group.absent(ctx, name=resource_id)
    assert f"'{resource_id}' already absent" in ret["comment"]
