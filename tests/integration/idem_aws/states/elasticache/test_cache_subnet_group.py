import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_cache_subnet_group(hub, ctx, aws_ec2_subnet):
    # Create cache subnet group
    cache_subnet_group_name = "idem-test-cache-subnet-" + str(uuid.uuid4())
    cache_subnet_group_description = "test-cache-subnet-description"
    tags = [{"Key": "Name", "Value": cache_subnet_group_name}]
    ret = await hub.states.aws.elasticache.cache_subnet_group.present(
        ctx,
        name=cache_subnet_group_name,
        cache_subnet_group_description=cache_subnet_group_description,
        subnet_ids=[
            aws_ec2_subnet.get("SubnetId"),
        ],
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource_id = ret["new_state"]["CacheSubnetGroupName"]

    # Describe cache subnet group
    describe_ret = await hub.states.aws.elasticache.cache_subnet_group.describe(ctx)

    if not hub.tool.utils.is_running_localstack(ctx):
        assert resource_id in describe_ret
        # Verify that the describe output format is correct
        assert "aws.elasticache.cache_subnet_group.present" in describe_ret.get(
            resource_id
        )
        described_resource = describe_ret.get(resource_id).get(
            "aws.elasticache.cache_subnet_group.present"
        )
        described_resource_map = dict(ChainMap(*described_resource))
        if described_resource_map.get("tags") is not None:
            assert tags == described_resource_map.get("tags")

    # Delete cache subnet group
    ret = await hub.states.aws.elasticache.cache_subnet_group.absent(
        ctx, name=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.elasticache.cache_subnet_group.absent(
        ctx, name=resource_id
    )
    assert f"'{resource_id}' already absent" in ret["comment"]
